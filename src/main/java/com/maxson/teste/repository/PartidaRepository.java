package com.maxson.teste.repository;

import com.maxson.teste.domain.Partida;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PartidaRepository extends JpaRepository<Partida, Long> {
    List<Partida> findAllByJogoId(Long id);
}
