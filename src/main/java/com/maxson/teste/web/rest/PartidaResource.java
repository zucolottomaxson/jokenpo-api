package com.maxson.teste.web.rest;

import com.maxson.teste.service.PartidaService;
import com.maxson.teste.service.dto.PartidaDTO;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api/partida")
@AllArgsConstructor
public class PartidaResource {
    private final Logger log = LoggerFactory.getLogger(PartidaResource.class);


    private final PartidaService service;

    @PostMapping()
    public ResponseEntity<PartidaDTO> createPartida(@RequestBody PartidaDTO partidaDTO) throws URISyntaxException {
        log.debug("REST request to save partida : {}", partidaDTO);
        return ResponseEntity.ok(service.salvar(partidaDTO));
    }

    @GetMapping("/jogo/{id}")
    public ResponseEntity<List<PartidaDTO>> buscarPartidas(@PathVariable(name = "id")Long idJogo) throws URISyntaxException {
        log.debug("REST request to get User");
        return ResponseEntity.ok(service.buscarTodasPartidasJogo(idJogo));
    }

}
