package com.maxson.teste.web.rest;

import com.maxson.teste.service.JogoService;
import com.maxson.teste.service.PartidaService;
import com.maxson.teste.service.dto.JogoDTO;
import com.maxson.teste.service.enuns.VencedorEnum;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/jogo")
@AllArgsConstructor
public class JogoResource {
    private final Logger log = LoggerFactory.getLogger(JogoResource.class);


    private final JogoService service;

    private final PartidaService partidaService;

    @PostMapping()
    public ResponseEntity<JogoDTO> salvar(@Valid @RequestBody JogoDTO jogoDTO) throws URISyntaxException {
        log.debug("REST request to save Game : {}", jogoDTO);
        return ResponseEntity.ok(service.salvar(jogoDTO));
    }

    @GetMapping()
    public ResponseEntity<List<JogoDTO>> buscar() throws URISyntaxException {
        log.debug("REST request to get User");
        return ResponseEntity.ok(service.buscarTodoJogos());
    }


    @GetMapping("/resultado/{id}")
    public ResponseEntity<Map<VencedorEnum,Long>> resultadoFinal(@PathVariable(name = "id")Long id) throws URISyntaxException {
        log.debug("REST request to get User");
        return ResponseEntity.ok(partidaService.buscarTodasvencedorJogo(id));
    }


}
