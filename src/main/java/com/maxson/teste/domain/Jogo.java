package com.maxson.teste.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A user.
 */
@Entity
@Table(name = "jogo")
@Getter
@Setter
public class Jogo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(nullable = false)
    private String participante;

    @NotNull
    @Column(nullable = false)
    private Long quantidadeRodadas;
}
