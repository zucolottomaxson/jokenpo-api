package com.maxson.teste.domain;

import com.maxson.teste.service.enuns.JogadaEnum;
import com.maxson.teste.service.enuns.VencedorEnum;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * A user.
 */
@Entity
@Table(name = "partida")
@Getter
@Setter
public class Partida implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private JogadaEnum jogadaUsuario;


    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private JogadaEnum JogadaBot;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private VencedorEnum vencedor;

    @ManyToOne
    @JoinColumn(name = "jogo_id", referencedColumnName = "id")
    private Jogo jogo;

}
