package com.maxson.teste.service;

import com.maxson.teste.repository.JogoRepository;
import com.maxson.teste.service.dto.JogoDTO;
import com.maxson.teste.service.mapper.JogoMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service class for managing users.
 */
@Service
@Transactional
@RequiredArgsConstructor
public class JogoService {
    private final Logger log = LoggerFactory.getLogger(JogoService.class);

    private final JogoRepository jogoRepository;

    private final JogoMapper jogoMapper;

    public JogoDTO salvar(JogoDTO jogoDTO){
        return jogoMapper.toDto(jogoRepository.save(jogoMapper.toEntity(jogoDTO)));
    }

    public List<JogoDTO> buscarTodoJogos(){
        return jogoMapper.toDto(jogoRepository.findAll());
    }

}
