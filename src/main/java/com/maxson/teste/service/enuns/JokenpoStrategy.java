package com.maxson.teste.service.enuns;

public interface JokenpoStrategy {
    VencedorEnum ganhaDe(JogadaEnum jogada);
}
