package com.maxson.teste.service.enuns;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum JogadaEnum implements JokenpoStrategy {
    PEDRA(1L){
        @Override
        public VencedorEnum ganhaDe(JogadaEnum jogadaBot) {
            return jogadaBot.equals(JogadaEnum.TESOURA)?VencedorEnum.PLAYER:VencedorEnum.BOT;
        }
    }, PAPEL(2L){
        @Override
        public VencedorEnum ganhaDe(JogadaEnum jogada) {
            return jogada.equals(JogadaEnum.PEDRA)?VencedorEnum.PLAYER:VencedorEnum.BOT;
        }
    }, TESOURA(3L){
        @Override
        public VencedorEnum ganhaDe(JogadaEnum jogada) {
            return jogada.equals(JogadaEnum.PAPEL)?VencedorEnum.PLAYER:VencedorEnum.BOT;
        }
    };
    private Long value;

    JogadaEnum(Long value){
        this.value = value;
    }

    public static JogadaEnum obterEnum(Long valorTeste){
        return Arrays.stream(JogadaEnum.values()).filter(jogadaEnum -> {
           return jogadaEnum.getValue().equals(valorTeste);
        }).findFirst().get();
    }
}
