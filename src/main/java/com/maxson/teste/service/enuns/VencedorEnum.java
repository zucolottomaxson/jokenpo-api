package com.maxson.teste.service.enuns;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum VencedorEnum {
    PLAYER, BOT, EMPATE
}
