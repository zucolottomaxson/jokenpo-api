package com.maxson.teste.service.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A user.
 */
@Getter
@Setter
public class JogoDTO implements Serializable {
    private Long id;
    private String participante;
    private Long quantidadeRodadas;
}
