package com.maxson.teste.service.dto;

import com.maxson.teste.service.enuns.JogadaEnum;
import com.maxson.teste.service.enuns.VencedorEnum;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class PartidaDTO implements Serializable {

    private Long id;

    private JogadaEnum jogadaUsuario;

    private JogadaEnum JogadaBot;

    private VencedorEnum vencedor;

    private Long jogoId;

}
