package com.maxson.teste.service.mapper;

import com.maxson.teste.domain.Partida;
import com.maxson.teste.service.dto.PartidaDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;



@Mapper(componentModel = "spring")
public interface PartidaMapper {
    @Mapping(target = "jogoId", source = "jogo.id")
    PartidaDTO toDto(Partida partida);
    @Mapping(target = "jogo.id", source = "jogoId")
    Partida toEntity(PartidaDTO partidaDTO);

    List<PartidaDTO> toDto(List<Partida> partidaList);
}
