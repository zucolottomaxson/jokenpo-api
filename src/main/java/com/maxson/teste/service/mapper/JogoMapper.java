package com.maxson.teste.service.mapper;

import com.maxson.teste.domain.Jogo;
import com.maxson.teste.service.dto.JogoDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface JogoMapper {
    JogoDTO toDto(Jogo jogo);
    Jogo toEntity(JogoDTO jogoDTO);
    List<JogoDTO> toDto(List<Jogo> jogoList);
}
