package com.maxson.teste.service;

import com.maxson.teste.repository.PartidaRepository;
import com.maxson.teste.service.dto.PartidaDTO;
import com.maxson.teste.service.enuns.JogadaEnum;
import com.maxson.teste.service.enuns.VencedorEnum;
import com.maxson.teste.service.mapper.PartidaMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Service class for managing users.
 */
@Service
@Transactional
@RequiredArgsConstructor
public class PartidaService {
    private final Logger log = LoggerFactory.getLogger(PartidaService.class);

    private final PartidaRepository repository;

    private final PartidaMapper mapper;

    public PartidaDTO salvar(PartidaDTO partidaDTO){
        partidaDTO.setJogadaBot(obterJogadaBot());
        partidaDTO.setVencedor(obtervencedorPartida(partidaDTO.getJogadaUsuario(),partidaDTO.getJogadaBot()));
        return mapper.toDto(repository.save(mapper.toEntity(partidaDTO)));
    }

    private JogadaEnum obterJogadaBot(){
        Random r = new Random();
        Long valor = Long.valueOf(r.nextInt(3));
        valor++;
        return JogadaEnum.obterEnum(valor);
    }

    private VencedorEnum obtervencedorPartida(JogadaEnum jogaPlayer, JogadaEnum jogadaBot){
        return jogaPlayer.equals(jogadaBot)?VencedorEnum.EMPATE:jogaPlayer.ganhaDe(jogadaBot);
    }

    public List<PartidaDTO> buscarTodasPartidasJogo(Long idJogo){
        return mapper.toDto(repository.findAllByJogoId(idJogo));
    }

    public Map<VencedorEnum, Long> buscarTodasvencedorJogo(Long idJogo){
        List<PartidaDTO> partidas = mapper.toDto(repository.findAllByJogoId(idJogo));
        Map<VencedorEnum, Long> total = partidas.stream()
            .collect(Collectors.groupingBy(PartidaDTO::getVencedor, Collectors.counting()));
        return total;
    }

}
