import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { AplicacaoTesteSharedModule } from 'app/shared/shared.module';
import { AplicacaoTesteCoreModule } from 'app/core/core.module';
import { AplicacaoTesteAppRoutingModule } from './app-routing.module';
import { AplicacaoTesteHomeModule } from './home/home.module';
import { AplicacaoTesteEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    AplicacaoTesteSharedModule,
    AplicacaoTesteCoreModule,
    AplicacaoTesteHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    AplicacaoTesteEntityModule,
    AplicacaoTesteAppRoutingModule,
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, FooterComponent],
  bootstrap: [MainComponent],
})
export class AplicacaoTesteAppModule {}
